#/bin/bash

docker container rm -f $(docker container ls -qa)

docker image rm -f $(docker image ls -qa)

#docker container run -dit -p 90:80 jadilsonpaiva/fabrica

docker run --name zabbix-appliance -p 90:80 -p 10051:10051 -v /var/lib/mysql:/var/lib/mysql -d zabbix/zabbix-appliance
